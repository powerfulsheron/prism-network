import { Message } from '@/models/Message';

export interface Group {
  id: string;
  name: string;
  messages: Message[];
}
