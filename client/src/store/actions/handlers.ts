import { State } from '@/store/state';
import { Store, ActionTree, ActionContext } from 'vuex';
import {
  LoginMutation,
  LoginSuccessMutation,
  LoadMessageSuccessMutation,
  LoadMessagesMutation,
  SelectGroupMutation,
  LoadGroupsSuccessMutation,
} from '@/store/mutations';
import {
  LoginAction,
  LoadMessagesAction,
  SelectGroupAction,
  SELECT_GROUP,
  LOGIN,
  LOAD_MESSAGES,
  LoadGroupsAction,
  LOAD_GROUPS,
} from './actions';
import { getGroups, getMessages } from '@/api';

async function delay(x = 1000) {
  return await new Promise((res) => setTimeout(res, x));
}

type Context = ActionContext<State, State>;

export const actions = {
  async [LOGIN]({ commit, dispatch }: Context, { payload: { login, password } }: LoginAction): Promise<void> {
    console.log('Login action ', login, password);
    commit(new LoginMutation());
    await delay();
    commit(new LoginSuccessMutation());
  },
  async [LOAD_MESSAGES]({ commit }: Context, { payload: { groupId } }: LoadMessagesAction) {
    commit(new LoadMessagesMutation());
    const messages = await getMessages(groupId);
    commit(new LoadMessageSuccessMutation(messages, groupId));

  },
  async [SELECT_GROUP]({ commit, dispatch }: Context, { id }: SelectGroupAction) {
    commit(new SelectGroupMutation(id));
    await dispatch(new LoadMessagesAction({ groupId: id }));
  },
  async [LOAD_GROUPS]({ commit }: Context, action: LoadGroupsAction) {
    const groups = await getGroups();
    commit(new LoadGroupsSuccessMutation(groups));
  },
};
