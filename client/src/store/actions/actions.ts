
export const LOGIN = 'LOGIN';
export const LOAD_MESSAGES = 'LOAD_MESSAGES';
export const SELECT_GROUP = 'SELECT_GROUP';
export const LOAD_GROUPS = 'LOAD_GROUPS';


export class LoginAction {
  public readonly type = LOGIN;
  constructor(public readonly payload: { login: string, password: string }) { }
}
export class LoadMessagesAction {
  public readonly type = LOAD_MESSAGES;
  constructor(public readonly payload: { groupId: string }) { }
}
export class SelectGroupAction {
  public readonly type = SELECT_GROUP;
  constructor(public readonly id: string) { }
}
export class LoadGroupsAction {
  public readonly type = LOAD_GROUPS;
}
