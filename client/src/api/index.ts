import { Group } from '@/models/Group';
import { groups, messages } from '@/api/mock';
import { Message } from '@/models/Message';



export function getGroups(): Promise<Group[]> {

  return Promise.resolve(groups);
}

export function getGroup(groupId: string): Promise<Group> {
  const group = groups.find((g) => g.id === groupId);
  if (group) {
    return Promise.resolve(group);
  } else { return Promise.reject(new Error('Not found')); }
}

export function getMessages(groupId: string): Promise<Message[]> {

  return Promise.resolve(messages[groupId]);
}

export function postMessage(message: Message) {
  throw new Error('Not implemented');
}
