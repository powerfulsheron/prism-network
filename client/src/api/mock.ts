import { Group } from '@/models/Group';
import { Message } from '@/models/Message';

export const groups: Group[] = [
  { name: 'Group 1', id: 'group1', messages: [] },
  { name: 'Group 2', id: 'group2', messages: [] },
];
export const messages: { [key: string]: Message[] } = {
  group1: [
    { id: 'msg1', message: 'Hello', date: new Date() },
    { id: 'msg2', message: 'Bonjour', date: new Date() },
    { id: 'msg3', message: 'Hola', date: new Date() },
  ],
  group2: [
    { id: 'msg2', message: 'Hi', date: new Date() },
  ],
};
