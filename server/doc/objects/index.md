# The Mioga objects

## Classes documentation

- [User](user.md)

## Using classes

Mioga classes should be used this way:

- load module:

```
import ClassName from './objects/ClassName';
```

- create an object:

```
var obj = new ClassName(mioga, {
	// place relevant object attributes here
});
```

- load an object:

```
obj.load()
	.then(() => {
		// place relevent success code here
	})
	.catch(error => {
		// place relevent error-handling code here
	});
```

- store (create or update) an object:

```
obj.store()
	.then(() => {
		// place relevent success code here
	})
	.catch(error => {
		// place relevent error-handling code here
	});
```

## Adding classes

Mioga provides a simplified development environment allowing one to easilly extend it.

This page describes how a "`MyClass`" class should be implemented.

Adding a class to Mioga is a 2-step operation:

- create the class itself to use it from Mioga,
- create the object-specific bindings for database access.

### Class definition

All the internal objects are ES6 classes that extend the "`BaseObject`" class.

The code should be placed into a dedicated file under the "`src/objects/`" directory.
The class should be defined this way:

```
'use strict';

import BaseObject from './BaseObject';

class MyClass extends BaseObject {
	init(attr){
		this.log.trace("[init] Entering");

		this.log.trace("[init] Leaving");
	}

	// Getters & setters
}

export default MyClass;
```

When an object is instantiated, the "`init()`" method is automatically called. It receives the object attributes as argument.

You should add your own getters and setters to your object.

### Database bindings

The database bindings consist of a set of files (one for each supported database driver) under the "`src/objects/db-bindings/`" directory.

#### Database-specific bindings

These bindings are common to all classes. They only provide general-purpose methods like "`load`" and "`store`".

These methods are automatically added to the class and can be called, like each class method, through "`this`":

```
this.load();
```

#### Object-specific bindings

These bindings are object-specific and must be defined by the developer who is extending Mioga by adding an class.

The naming of these files is "`<class_name>.<database_type>.js`".

They are mainly aimed at providing the matching schema into "`this._model`" attribute. They must provide at least a method named "`schema`" taking no argument. **This method must define "`this._model`".**

Any other initialization can be placed into this method.

These bindings can also define other methods needed by the developer.

These methods are automatically added to the class and can be called, like each class method, through "`this`":

```
this.my_custom_method();
```
