/************************************************************
 *
 * User
 *
 *    This class handles a single user or a list of users.
 *
 ************************************************************/

'use strict';

import BaseObject from './BaseObject';

class User extends BaseObject {
	init(attr){
		this.log.trace("[init] Entering");

		this.log.trace("[init] Leaving");
	}

	get firstname() { return this._attr.firstname };
	set firstname(val) { this._attr.firstname = val};

	get lastname() { return this._attr.lastname };
	set lastname(val) { this._attr.lastname = val};

	get email() { return this._attr.email };
	set email(val) { this._attr.email = val};
}

export default User;
