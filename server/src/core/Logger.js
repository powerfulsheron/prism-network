/************************************************************
 *
 * Logger
 *
 *    This is a basic but useful logging system that handles
 *    multiple levels and automatically prefixes messages
 *    with caller method name.
 *
 ************************************************************/

'use strict';

export default class Logger {
	static default = 'ERROR'
	static levels = { TRACE: 0, DEBUG: 1, INFO: 2, WARN: 3, ERROR: 4, SILENT: 5 }

	constructor(mioga, className) {
		this.className = className;

		if(mioga.get(`log.${this.className}`)) {
			this.level = mioga.get(`log.${this.className}`);
		}
		else {
			this.level = Logger.default
		}
	}

	// Extract caller function name from stack trace
	get caller() {
		return `${this.className}::${new Error().stack.split("\n")[3].match(/^[ ]* at ([^ ]*) .*$/)[1].replace(new RegExp(`^${this.className}\.`), "")}`;
	}

	trace(msg) {
		if (Logger.levels[this.level] > Logger.levels.TRACE) return;
		console.log(`[${this.caller}] ${msg}`);
	}

	debug(msg) {
		if (Logger.levels[this.level] > Logger.levels.DEBUG) return;
		console.log(`[${this.caller}] ${msg}`);
	}

	info(msg) {
		if (Logger.levels[this.level] > Logger.levels.INFO) return;
		console.log(`[${this.caller}] ${msg}`);
	}

	warn(msg) {
		if (Logger.levels[this.level] > Logger.levels.WARN) return;
		console.warn(`[${this.caller}] ${msg}`);
	}

	error(msg) {
		if (Logger.levels[this.level] > Logger.levels.ERROR) return;
		console.error(`[${this.caller}] ${msg}`);
	}
}
