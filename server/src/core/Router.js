/************************************************************
 *
 * Router
 *
 *    This is the URL router.
 *
 ************************************************************/

'use strict';

import Logger from '../core/Logger';
var express = require('express');
var router = express.Router();

class Router {
	constructor(mioga, app) {
		// Mioga commons
		this._config = mioga;
		this._db = mioga.db;

		// Logging system
		this.log = new Logger(mioga, this.constructor.name);

		// Load routes
		this._load();

		// Enrich "req" object with "Mioga" object
		app.use(function(req, res, next){
			req.mioga = mioga;
			next();
		});

		// Register middleware router
		app.use(router);
	}

	// Load routes
	_load() {
		this._routes = require('../routes/');

		for (var domain in this._routes) {
			this.log.trace(`Loading routes for "${domain}"`);
			for (var url in this._routes[domain]) {
				for (var verb in this._routes[domain][url]) {
					this.log.trace(`Got ${verb.toUpperCase()} route for "/${domain}${url}"`);
					this._register_route(verb.toUpperCase(), `/${domain}${url}`, this._routes[domain][url][verb]);
				}
			}
		}
	}

	// Register a route
	_register_route(verb, path, handler) {
		switch (verb) {
			case 'GET':
				router.get(path, handler);
				break;
			case 'POST':
				router.post(path, handler);
				break;
			case 'PUT':
				router.put(path, handler);
				break;
			case 'DELETE':
				router.delete(path, handler);
				break;
			default:
				this.log.error(`${verb} HTTP method is not supported, please fix.`);
		}
	}
}

module.exports = Router;
